import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

        //primärer hash benutzt die XOR methode aus dem skript
        int l = 32; //maximale wortlänge 16*32 bits => 64 characters
        int[][] T = new int[l][256];
        for(int i=0; i<l; i++)
            for(int j=0; j<T[i].length; j++)
                T[i][j] = ThreadLocalRandom.current().nextInt();    //initialisiere matrix T durch zufällige werte aus {0,1}^32

        HashMap.HashFunction<String> primaryHash = (key, m)->{
            int sum = 0;
            for(int i=0; i<key.length(); i++){
                int c = key.charAt(i);
                sum = sum ^ T[i][c];   //wörter XOR-en
            }

            return (sum & 0x7FFFFFFF) % m;   //setze erstes bit auf null => sum ist >= 0, mod m
        };

        HashMap.HashFunction<String> secondaryHash = (key, m)->{
            int sum = 0;
            for(char c : key.toCharArray())
                sum += c;
            return 1+ sum % (m-2);
        };

        HashMap<String, String> name2Club = new HashMap<>(primaryHash, secondaryHash);
        HashMap<String, ArrayList<String>> club2Name = new HashMap<>(primaryHash, secondaryHash);

        for(Player player : Euro2008.allTeamPlayer){
            name2Club.insert(player.name, player.club);
            ArrayList<String> players = club2Name.search(player.club);
            if(players == null) {
                players = new ArrayList<>();
                players.add(player.name);
                club2Name.insert(player.club, players);
            }else
                players.add(player.name);
        }

        //input
        Scanner scanner = new Scanner(System.in); // Make scanner obj
        String searchName;
        while((searchName = scanner.nextLine()) != null) {
            String club = name2Club.search(searchName);
            if (club == null) {
                System.out.println("No player named " + searchName + " was found");
                return;
            }
            System.out.println("club: "+club);
            System.out.println("players: ");
            ArrayList<String> players = club2Name.search(club);
            System.out.print(players.get(0));
            for (int i=1; i<players.size(); i++)
                System.out.print(", "+players.get(i));
            System.out.println();
        }
    }
}
