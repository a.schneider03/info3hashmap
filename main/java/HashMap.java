import java.util.Arrays;


public class HashMap<K extends Comparable<K>, V>{

    //Container für Knoten
    private static class Node<K, V> {
        //false by default
        boolean deleted;
        V val;
        K key;

        private Node(K key, V val) {
            this.val = val;
            this.key = key;
        }
    }

    //Interface zum implementieren der Hashfunktionen
    public interface HashFunction<I>{
        int hash(I key, int m);
    }

    //Wie viele elemente sind in der Hashmap
    private int itemCount;
    //Wie viele Buckets sind besetzt
    private int bucketCount;
    //Maximale auslastung der Tabelle
    private double maxAlpha = 0.75;
    //Minimale auslastung der Tabelle
    //gilt nicht minAlpha < alpha < maxAlpha, so wird die größe der Tabelle angepasst
    private double minAlpha = 0.25;
    private HashFunction<K> primaryHash;
    private HashFunction<K> secondaryHash;
    private Node<K, V>[] nodes;
    // primes sind mögliche längen der HashMap
    // Zahlen kommen von https://planetmath.org/goodhashtableprimes
    private int[] primes = new int[]{29,53,97,193,389,769,1543,3079,6151,12289,24593,49157,98317,196613,393241,786433,1572869,3145739,6291469,12582917,25165843,50331653,100663319,201326611,402653189,805306457,1610612741};
    private int sizeIndex;

    //Construktor, da datentyp generic ist müssen hashfunktionen mit übergeben werden
    public HashMap(HashFunction<K> h1, HashFunction<K> h2) {
        primaryHash = h1;
        secondaryHash = h2;
        nodes = new Node[primes[sizeIndex]];
    }

    //Füge(key, value) in array ein, checkExpand gibt an ob die map überprüft werden soll zu vergrößern
    private void insert(K key, V value, Node<K, V>[] array, boolean checkExpand){
        int h1 = primaryHash.hash(key, array.length);
        int h2 = secondaryHash.hash(key, array.length);

        for(int i=0; i<array.length; i++){          //Durchlaufe die Permutation
            int h = getHash(h1, h2, i, array.length);
            if(array[h] == null || array[h].deleted) {       //Bucket ist leer oder als gelöscht markiert -> insert
                if(array[h] == null)  bucketCount++;        //Bucket war leer => erhöhe count
                array[h] = new Node<K, V>(key, value);
                itemCount++;

                if(checkExpand && bucketCount > maxAlpha*array.length)   //alpha > maxAlpha => vergrößern
                    expand();
                return;
            }
        }
        // Sollte nicht eintreffen, da getHash(h1, h2, i) Permutation ist und immer für freie Buckets gesorgt ist
        System.out.println("Smth. went wrong, :=\\");
    }

    public void insert(K key, V value){
       insert(key, value, nodes, true);
    }

    //Hash-permutation, nodes.length ist stets primzahl
    private int getHash(int h1, int h2, int offset, int len){
        int res = (h1 + offset*h2) % len;
        return res;
    }

    //lösche key aus hashmap
    public void delete(K key){
        Node<K, V> n = searchNode(key);
        if(n != null) {
            n.deleted = true;
            itemCount--;
            if(sizeIndex > 0 && itemCount < minAlpha*nodes.length)  //ist alpha < minAlpha ? => map schrumpfen
                reduce();
        }
    }

    //Eigentliche suchfunktion, gibt den Knoten zum key aus
    private Node<K, V> searchNode(K key){
        int h1 = primaryHash.hash(key, nodes.length);
        int h2 = secondaryHash.hash(key, nodes.length);

        for(int i=0; i<nodes.length; i++){                  //Permutation wird durchlaufen
            Node<K,V> node = nodes[getHash(h1, h2, i, nodes.length)];
            if(node == null)                                //leeres Bucket getroffen
                return null;
            if(!node.deleted && node.key.compareTo(key) == 0)         //keys sind identisch, nicht markiert => element gefunden;
                return node;
        }
        return null;    //sollte nicht eintreffen
    }

    //sucht den Wert assoziiert mit key
    public V search(K key){
        Node<K, V> n = searchNode(key);
        if(n == null)   return null;
        return n.val;
    }

    //Funktion zum schrumpfen der Hashmap, darf nur gecallt werden wenn size > 0 gilt
    private void reduce() {
        Node<K, V>[] nodesCpy = new Node[primes[--sizeIndex]];  //kopie kleiner als nodes
        bucketCount = 0;
        itemCount = 0;
        for(Node<K, V> n : nodes){
            if(!n.deleted)
                insert(n.key, n.val, nodesCpy, false);   //nicht markierte werde werden in nodesCpy inserted
        }
        nodes = nodesCpy;
    }

    //Erhöht die größe der Hashmap,
    private void expand(){
        Node<K, V>[] nodesCpy;
        if(itemCount > minAlpha*primes[sizeIndex+1])    //neues alpha > minAlpha => hashmap wird vergrößert
            nodesCpy = new Node[primes[++sizeIndex]];
        else
            nodesCpy = new Node[primes[sizeIndex]];     //größe bleibt gleich, es werden nur markierte elemente gelöscht

        bucketCount = 0;
        itemCount = 0;
        for(Node<K, V> n : nodes){
            if(n != null && !n.deleted)
                insert(n.key, n.val, nodesCpy, false);  //nicht markierte werde werden in nodesCpy inserted
        }                                                           //checkExpand = false um endlosschleife zu vermeiden
        nodes = nodesCpy;
    }

    public int size(){
        return itemCount;
    }
}
